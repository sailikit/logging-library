/**
 * This is the module documentation comment for the Logger
 * module. You should replace this text with your own
 * documentation. Do not remove the "@module" directive at the
 * bottom of this comment, however: it tells TypeDoc that this
 * is a documentation comment for this whole module, not just
 * for some individual definition in the module.
 *
 * @module
 */

/* This is a dummy definition just so that this file has at
 * least one export, which is required for the test suite to
 * run. Delete this once you've defined at least one export of
 * your own in this file.
 */
//export const dummy = 0;

//estlint-disable-next-line @typescript-eslint/explicit-function-return-type
export const messageInfo = () => {
    console.info("information message");
};
//estlint-disable-next-line @typescript-eslint/explicit-function-return-type
export const messageWarning = () => {
    console.info("Warning message");
};
//estlint-disable-next-line @typescript-eslint/explicit-function-return-type
export const messageDebug = () => {
    console.info("Debug message");
};    

/**
 * 
 * @param message
 * Function log1 used for executing the pseudo code step by step
 * configure is used as a flag variable
 * when the flag is set to false log1 ignores the informationa level
 * log1 stores the new logger
 * log1 has a message as the parameter passed from the
 * logger.test.ts configure as a flag variable
 * Is displayed by the console
 * Info console method "test 1 " as message
 * when flag is set to true the information level
 * is displayed by the console. info console  method with "test1"
 * when flag is set to be False log 1 is configured to ignore the infromation level
 * As severity level is currenly ignored by log1
 * library does not produce any output response.
 * log 1 is used to call the logging function with message "test3" at warning level
 */