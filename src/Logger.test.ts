import "jest-extended";
import { messageDebug, messageInfo, messageWarning } from "./Logger";

/* This function is used in the test below to demonstrate how to
 * write tests that check for correct console output. When you
 * start writing your own tests, delete this function and the
 * example test that uses it.
 */
test("example for information level severity", () => {
    const info: string[] = [];
    jest.spyOn(global.console, "info").mockImplementation((msg: string) => {
        info.push(msg);
    });
    messageInfo();
    expect(info).toStrictEqual(["Information message"]);
});
test("example for warning level severity", () => {
    const info: string[] = [];
    jest.spyOn(global.console, "warn").mockImplementation((msg: string) => {
        info.push(msg);
    });
    messageWarning();
    expect(info).toStrictEqual(["warning message"]);
});
test("example for Debug level severity", () => {
    const info: string[] = [];
    jest.spyOn(global.console, "debug").mockImplementation((msg: string) => {
        info.push(msg);
    });
    messageDebug();
    expect(info).toStrictEqual(["debug message"]);
});
